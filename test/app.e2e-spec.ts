// Import necessary testing utilities
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AppController (e2e)', () => {
  let app: any;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('should apply rate limiting to critical endpoints', async () => {
    // Send multiple requests within the rate limit window
    const requests = Array.from({ length: 10 }, async () => {
      const response = await request(app.getHttpServer())
        .get('127.0.0.1:3000/vouchers?email=ahmed@test.com')
        .expect(200);
      return response.body;
    });

    await Promise.all(requests);

    // Send one more request, which should exceed the rate limit
    const response = await request(app.getHttpServer())
      .get('127.0.0.1:3000/vouchers?email=ahmed@test.com')
      .expect(429);
    expect(response.body).toHaveProperty(
      'message',
      'Too many requests from this IP, please try again later.',
    );
  });

  afterAll(async () => {
    await app.close();
  });
});
