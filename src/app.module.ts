import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './customers/customer.entity';
import { VoucherModule } from './vouchers/voucher.module';
import { Offer } from './offers/offers.entity';
import { Voucher } from './vouchers/voucher-code.entity';
import { OffersModule } from './offers/offers.module';
import { CustomersModule } from './customers/customers.module';
import { RateLimitMiddleware } from './middlewares/rate-limit.middleware';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    ThrottlerModule.forRoot([{ name: '', ttl: 1000, limit: 5 }]),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'voucherpool',
      entities: [Customer, Voucher, Offer],
      synchronize: true,
    }),
    VoucherModule,
    OffersModule,
    CustomersModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RateLimitMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
