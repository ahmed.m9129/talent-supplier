import { LoggerService } from '@nestjs/common';

export class CustomLoggerService implements LoggerService {
  log(message: string) {
    console.log(`[Custom Logger] ${message}`);
  }

  error(message: string, trace: string) {
    console.error(`[Custom Logger - ERROR] ${message} - ${trace}`);
  }

  warn(message: string) {
    console.warn(`[Custom Logger - WARNING] ${message}`);
  }

  debug(message: string) {
    console.debug(`[Custom Logger - DEBUG] ${message}`);
  }

  verbose(message: string) {
    console.log(`[Custom Logger - VERBOSE] ${message}`);
  }
}
