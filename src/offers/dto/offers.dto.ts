import {
  IsString,
  IsNumber,
  IsNotEmpty,
  IsPositive,
  Length,
  Max,
  Min,
} from 'class-validator';

export class CreateOffersDTO {
  @IsNotEmpty({ message: 'Name must not be empty' })
  @IsString({ message: 'Name must be a string' })
  @Length(4, 24)
  name: string;

  @IsNotEmpty({ message: 'Discount must not be empty' })
  @IsNumber({}, { message: 'Discount must be a number' })
  @IsPositive({ message: 'Discount must be a positive number' })
  @Max(100)
  @Min(0)
  discount: number;

  @IsNotEmpty({ message: 'Details must not be empty' })
  @IsString({ message: 'Details must be a string' })
  details: string;
}
