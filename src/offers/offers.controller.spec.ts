import { Test, TestingModule } from '@nestjs/testing';
import { OffersController } from './offers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Offer } from './offers.entity';
import { OffersService } from './providers/offers.service';
import { CreateOffersDTO } from './dto/offers.dto';
import { InternalServerErrorException } from '@nestjs/common';

describe('OffersController', () => {
  let controller: OffersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'postgres',
          password: 'postgres',
          database: 'voucherpool',
          entities: [Offer],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([Offer]),
      ],
      controllers: [OffersController],
      providers: [OffersService],
    }).compile();

    controller = module.get<OffersController>(OffersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return created offer when valid createOfferDTO is passed', async () => {
    const createOfferDto: CreateOffersDTO = {
      name: 'Test Offer',
      discount: 10,
      details: 'Test offer details',
    };

    const mockOffer: Offer = {
      id: 1,
      name: 'Test Offer',
      discount: 10,
      details: 'Test offer details',
    };

    jest
      .spyOn(OffersService.prototype, 'createOffer')
      .mockResolvedValue(mockOffer);

    const result = await controller.create(createOfferDto);

    expect(result).toEqual(mockOffer);
  });

  it('should return list of offers when listOffers is called', async () => {
    const mockOffers: Offer[] = [
      {
        id: 1,
        name: 'Test Offer 1',
        discount: 10,
        details: 'Test offer details 1',
      },
      {
        id: 2,
        name: 'Test Offer 2',
        discount: 20,
        details: 'Test offer details 2',
      },
    ];

    jest
      .spyOn(OffersService.prototype, 'listOffers')
      .mockResolvedValue(mockOffers);

    const result = await controller.list();

    expect(result).toEqual(mockOffers);
  });

  it('should return HTTP status code 201 for successful create request and list request', async () => {
    const createOfferDto: CreateOffersDTO = {
      name: 'Test Offer',
      discount: 10,
      details: 'Test offer details',
    };

    const mockOffer: Offer = {
      id: 1,
      name: 'Test Offer',
      discount: 10,
      details: 'Test offer details',
    };

    jest
      .spyOn(OffersService.prototype, 'createOffer')
      .mockResolvedValue(mockOffer);
    jest.spyOn(OffersService.prototype, 'listOffers').mockResolvedValue([]);

    const createResult = await controller.create(createOfferDto);
    expect(createResult).toEqual(mockOffer);

    const listResult = await controller.list();
    expect(listResult).toEqual([]);
  });

  it('should throw InternalServerErrorException when listOffers throws an error', async () => {
    jest
      .spyOn(OffersService.prototype, 'listOffers')
      .mockRejectedValue(new Error('Test error'));

    await expect(controller.list()).rejects.toThrow(
      InternalServerErrorException,
    );
  });
});
