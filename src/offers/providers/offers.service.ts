import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Offer } from '../offers.entity';
import { CreateOffersDTO } from '../dto/offers.dto';

@Injectable()
export class OffersService {
  constructor(
    @InjectRepository(Offer)
    private offersRepository: Repository<Offer>,
  ) {}

  async createOffer(createOfferDto: CreateOffersDTO): Promise<Offer> {
    try {
      const offer = this.offersRepository.create(createOfferDto);
      return this.offersRepository.save(offer);
    } catch (error) {
      throw error;
    }
  }

  async listOffers(): Promise<Offer[]> {
    try {
      const offers = this.offersRepository.find();
      return offers;
    } catch (error) {
      throw error;
    }
  }
}
