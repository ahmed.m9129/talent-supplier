import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Post,
} from '@nestjs/common';
import { CreateOffersDTO } from './dto/offers.dto';
import { OffersService } from './providers/offers.service';

@Controller('offers')
export class OffersController {
  constructor(private offerService: OffersService) {}
  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() createOfferDto: CreateOffersDTO) {
    try {
      const offer = await this.offerService.createOffer(createOfferDto);
      return offer;
    } catch (error) {
      throw new InternalServerErrorException('Error during creating offer');
    }
  }

  @Get('list')
  @HttpCode(HttpStatus.CREATED)
  async list() {
    try {
      const offers = await this.offerService.listOffers();
      return offers;
    } catch (error) {
      throw new InternalServerErrorException('Error during list offers');
    }
  }
}
