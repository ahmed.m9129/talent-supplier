import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class ValidateVoucherDTO {
  @IsNotEmpty({ message: 'email must not be empty' })
  @IsEmail({}, { message: 'email must be valid' })
  @IsString({ message: 'email must be a string' })
  email: string;

  @IsNotEmpty({ message: 'code must not be empty' })
  @IsString({ message: 'code must be a string' })
  @Length(4, 4, { message: 'code must be 4 characters legnth' })
  code: string;
}
