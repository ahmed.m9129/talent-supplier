import { IsNotEmpty, IsNumber, IsPositive, IsString } from 'class-validator';

export class CreateVoucherDTO {
  @IsNotEmpty({ message: 'Offer ID must not be empty' })
  @IsNumber(
    { allowNaN: false, allowInfinity: false },
    { message: 'Offer ID must be a number' },
  )
  @IsPositive({ message: 'Offer ID must not be negative' })
  offerId: number;

  @IsNotEmpty({ message: 'Customer ID must not be empty' })
  @IsNumber(
    { allowNaN: false, allowInfinity: false },
    { message: 'Customer ID must not be empty' },
  )
  @IsPositive({ message: 'Customer Id must not be negative' })
  customerId: number;

  @IsNotEmpty({ message: 'Value must not be empty' })
  @IsNumber({}, { message: 'Value must be a number' })
  @IsPositive({ message: 'Value must be not negative' })
  value: number;

  @IsNotEmpty({ message: 'expiration must not be empty' })
  @IsString({ message: 'Value must be a string' })
  expiration_date: string;
}
