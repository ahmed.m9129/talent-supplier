import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ListVoucherDTO {
  @IsNotEmpty({ message: 'Email must not be empty' })
  @IsString({ message: 'Email must a string' })
  @IsEmail({}, { message: 'Email must be valid' })
  email: string;
}
