import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Offer } from '../offers/offers.entity';
import { Customer } from '../customers/customer.entity';

@Entity()
export class Voucher {
  @PrimaryGeneratedColumn('increment')
  readonly id: number;

  @Column({ default: false, type: 'boolean' })
  is_used: boolean;

  @Column({ unique: true, nullable: false, type: 'varchar' })
  code: string;

  @Column({ type: 'int', nullable: true })
  @ManyToOne(() => Offer)
  @JoinColumn()
  offerId: number;

  @Column({ type: 'int', nullable: false })
  @ManyToOne(() => Customer)
  @JoinColumn()
  customerId: number;

  @Column({ type: 'date', nullable: true })
  expiration_date: Date;

  @Column({ type: 'float', nullable: false })
  value: number;

  @Column({ default: new Date(), nullable: true })
  readonly created_at: Date;

  @Column({ nullable: true })
  readonly updated_at: Date;
}
