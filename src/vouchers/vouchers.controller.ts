import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CreateVoucherDTO } from './dto/create-voucher.dto';
import { VouchersService } from './providers/vouchers.service';
import { ValidateVoucherDTO } from './dto/validate-voucher.dto';
import { ListVoucherDTO } from './dto/list-voucher.dto';

@Controller('vouchers')
export class VouchersController {
  constructor(private readonly vouchersService: VouchersService) {}
  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createVoucherDto: CreateVoucherDTO) {
    try {
      const voucher = this.vouchersService.generateVoucher(createVoucherDto);
      return voucher;
    } catch (error) {
      throw new InternalServerErrorException('Failed to create voucher');
    }
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  listVouchers(@Query() query: ListVoucherDTO) {
    const { email } = query;

    const vouchers = this.vouchersService.listVouchersByEmail(email);
    return vouchers;
  }

  @Put('validate')
  @HttpCode(HttpStatus.OK)
  validate(@Body() validateVoucherDto: ValidateVoucherDTO) {
    const voucher = this.vouchersService.validateVoucher(validateVoucherDto);

    return voucher;
  }
}
