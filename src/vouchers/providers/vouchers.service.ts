import * as moment from 'moment';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Voucher } from '../voucher-code.entity';
import { CreateVoucherDTO } from '../dto/create-voucher.dto';
import { ValidateVoucherDTO } from '../dto/validate-voucher.dto';
import { Offer } from '../../offers/offers.entity';
import { UtilsService } from './utils.service';
import { isEmail } from 'class-validator';

@Injectable()
export class VouchersService {
  constructor(
    @InjectRepository(Voucher)
    private voucherRepository: Repository<Voucher>,
    @InjectRepository(Offer)
    private offerRepository: Repository<Offer>,
    private readonly utilsService: UtilsService,
    private dataSource: DataSource,
  ) {}

  async generateVoucher(createVoucherDto: CreateVoucherDTO): Promise<Voucher> {
    try {
      const { customerId, offerId, value, expiration_date } = createVoucherDto;

      const code = this.utilsService.generateRandomCode();
      const voucher = await this.voucherRepository.findOneBy({ code });

      if (voucher) {
        throw new BadRequestException('Voucher is already exist');
      }
      const expirationDate = moment(
        expiration_date,
        ['YYYY-MM-DD', 'DD-MM-YYYY'],
        true,
      );

      if (!expirationDate.isValid()) {
        throw new BadRequestException('Invalid date format');
      }

      const newVoucher = this.voucherRepository.create({
        customerId,
        offerId,
        code,
        value,
        expiration_date: expirationDate.toDate(),
      });

      return this.voucherRepository.save(newVoucher);
    } catch (error) {
      throw error;
    }
  }

  async listVouchersByEmail(email: string): Promise<Voucher[]> {
    if (!isEmail(email)) {
      throw new BadRequestException('Invalid email address');
    }

    const vouchers = await this.voucherRepository
      .createQueryBuilder('voucher')
      .leftJoinAndSelect('voucher.customerId', 'customer')
      .where('customer.email = :email', { email })
      .innerJoinAndSelect('voucher.offerId', 'offer')
      .select(['voucher', 'offer.name'])
      .getMany();

    return vouchers;
  }

  async validateVoucher(
    validateVoucherDto: ValidateVoucherDTO,
  ): Promise<Voucher> {
    const { code, email } = validateVoucherDto;

    let voucher: Voucher;
    const queryRunner = this.dataSource.createQueryRunner();

    try {
      await queryRunner.connect();
      await queryRunner.startTransaction();

      voucher = await this.voucherRepository
        .createQueryBuilder('voucher')
        .where('voucher.code = :code', { code }) // Filter by voucher code
        .andWhere('voucher.is_used = false') // Check if the voucher is not used
        .andWhere('voucher.expiration_date >= :today', {
          today: moment().toDate(),
        })
        .leftJoin('voucher.customerId', 'customer')
        .andWhere('customer.email = :email', { email }) // Filter by customer email
        .getOne();

      if (!voucher) {
        throw new NotFoundException('Voucher not found');
      }

      voucher.is_used = true;
      await queryRunner.manager.save(voucher);

      const offer = await this.offerRepository.findOneBy({
        id: voucher.offerId,
      });

      if (!offer) {
        throw new BadRequestException('Offer not found');
      }

      // Apply discount
      voucher.value -= (offer.discount / 100) * voucher.value;
      await queryRunner.manager.save(voucher);

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw error;
    } finally {
      await queryRunner.release();
    }

    return voucher;
  }
}
