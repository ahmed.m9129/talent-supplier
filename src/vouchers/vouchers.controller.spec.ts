import { Test, TestingModule } from '@nestjs/testing';
import { VouchersController } from './vouchers.controller';
import { Voucher } from './voucher-code.entity';
import { VouchersService } from './providers/vouchers.service';
import { UtilsService } from './providers/utils.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Offer } from '../offers/offers.entity';
import { Customer } from '../customers/customer.entity';

describe('VouchersController', () => {
  let controller: VouchersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'postgres',
          password: 'postgres',
          database: 'voucherpool',
          entities: [Customer, Voucher, Offer],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([Voucher, Offer]),
      ],
      controllers: [VouchersController],
      providers: [VouchersService, UtilsService],
    }).compile();

    controller = module.get<VouchersController>(VouchersController);
  });

  it('test creating voucher with invalid offer ID', async () => {
    try {
      await controller.create({
        customerId: 1,
        expiration_date: '2024-02-17',
        offerId: null, // Invalid offer ID
        value: 1000,
      });

      fail('Voucher creation should have failed due to invalid offer ID');
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('test creating voucher with negative value', async () => {
    try {
      await controller.create({
        customerId: 1,
        expiration_date: '2024-02-17',
        offerId: 1,
        value: -100, // Negative value
      });

      fail('Voucher creation should have failed due to negative value');
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('test creating voucher with missing customer ID', async () => {
    try {
      await controller.create({
        customerId: null, // Missing customer ID
        expiration_date: '2024-02-17',
        offerId: 1,
        value: 1000,
      });

      fail('Voucher creation should have failed due to missing customer ID');
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('test validating voucher with valid data', async () => {
    try {
      await controller.validate({
        code: 'ABCD',
        email: 'test@example.com',
      });

      fail('Voucher validation should have failed due to valid data');
    } catch (error) {
      expect(error).toBeDefined();
    }
  });

  it('should return a list of vouchers when a valid email is provided', () => {
    const query = { email: 'valid@example.com' };
    const result = controller.listVouchers(query);
    expect(result).toBeInstanceOf(Promise<Voucher[]>);
  });
});
