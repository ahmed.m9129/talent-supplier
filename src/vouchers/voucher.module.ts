import { Module } from '@nestjs/common';
import { VouchersService } from './providers/vouchers.service';
import { VouchersController } from './vouchers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Voucher } from './voucher-code.entity';
import { UtilsService } from './providers/utils.service';
import { Offer } from '../offers/offers.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Voucher, Offer])],
  providers: [VouchersService, UtilsService],
  controllers: [VouchersController],
})
export class VoucherModule {}
