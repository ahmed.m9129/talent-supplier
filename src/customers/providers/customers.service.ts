import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from '../customer.entity';
import { CreateCustomerDTO } from '../dto/customers.dto';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}

  async createCustomer(createCustomerDto: CreateCustomerDTO) {
    try {
      const customer = await this.customerRepository.findOneBy({
        email: createCustomerDto.email,
      });
      if (customer) {
        throw new BadRequestException(['This email is in use']);
      }
      const newCustomer = this.customerRepository.create(createCustomerDto);
      return this.customerRepository.save(newCustomer);
    } catch (error) {
      throw error;
    }
  }

  async listCustomers() {
    try {
      const customers = await this.customerRepository.find();
      return customers;
    } catch (error) {
      throw error;
    }
  }
}
