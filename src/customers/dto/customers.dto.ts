import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateCustomerDTO {
  @IsNotEmpty({ message: 'name must be provided' })
  @IsString({ message: 'name must be string' })
  @Length(4, 24)
  name: string;

  @IsNotEmpty({ message: 'email must be provided' })
  @IsString({ message: 'email must be string' })
  @IsEmail({}, { message: 'email must be a valid email address' })
  email: string;
}
