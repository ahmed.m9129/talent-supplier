import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  InternalServerErrorException,
  Post,
} from '@nestjs/common';
import { CreateCustomerDTO } from './dto/customers.dto';
import { CustomersService } from './providers/customers.service';

@Controller('customers')
export class CustomersController {
  constructor(private customersService: CustomersService) {}
  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createCustomerDto: CreateCustomerDTO) {
    try {
      const customer = this.customersService.createCustomer(createCustomerDto);
      return customer;
    } catch (error) {
      throw new InternalServerErrorException('Failed to create customer');
    }
  }

  @Get('list')
  @HttpCode(HttpStatus.OK)
  async list() {
    try {
      const customers = await this.customersService.listCustomers();
      return customers;
    } catch (error) {
      throw new InternalServerErrorException('Failed to list customers');
    }
  }
}
